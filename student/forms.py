
from django import forms
from django.forms import ModelForm
from django.core.files.images import get_image_dimensions
from django.contrib.auth.models import User
from models import *


class UserForm(forms.ModelForm):
	confirmPassword = forms.CharField(min_length=8)
	
	class Meta:
		model = User
		fields = ('username', 'password')

	def clean_username(self):
		username = self.cleaned_data['username']

		try:
			User.objects.get(username = username)
			raise forms.ValidationError("That username is already taken"+username)

		except User.DoesNotExist:

			return username

	def clean_password(self):
		password = self.cleaned_data.get('password', '')
		#pswd1 = self.cleaned_data['password']
		if len(password) < 8:
			raise forms.ValidationError("Password should be atleast 8 characters ")
		"""if pswd != pswd1:
			raise forms.ValidationError("Passwords do not match") """
		return password	
	
	"""def clean_email(self):
		email_id = self.cleaned_data["email"]
		try:
			user_id = User.objects.get(email = email_id)
			raise forms.ValidationError("That email is already taken")
		except User.DoesNotExist:
			return email_id """



class StudentForm(forms.ModelForm):

	class Meta:
		model = Student
		exclude = ('user',)

class editphotoForm(forms.ModelForm):

	class Meta:
		model = Student
        fields = ('avatar',)

	"""def clean_avatar(self):

		avatar = self.cleaned_data['avatar']

    
		w, h = get_image_dimensions(avatar)

    #validate dimensions
        max_width = max_height = 120
        if w != max_width or h != max_height:
            raise forms.ValidationError(
                u'Please use an image that is '
                 '%s x %s pixels.' % (max_width, max_height))

        #validate content type
        main, sub = avatar.content_type.split('/')
        if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
            raise forms.ValidationError(u'Please use a JPEG, '
                'GIF or PNG image.')

        #validate file size
        if len(avatar) > (20 * 1024):
        	raise forms.ValidationError(u'Avatar file size may not exceed 20k.')

		return avatar

		"""

class SkillsForm(forms.ModelForm):

	class Meta:
		model = Skills
		fields = ['skills', ]

class ExperienceForm(forms.ModelForm):

	class Meta:
		model = Experience
		fields = ['company','profile', 'location','duration',]







    
