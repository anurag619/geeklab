
from django.db import models
from django.contrib.auth.models import User

class Student(models.Model):

	user = models.ForeignKey(User, unique=True)
	#chapter = models.CharField(max_length=20)
	name = models.CharField(max_length=30, blank=True)
	homepage_url = models.URLField(max_length=50, blank=True)
	avatar = models.ImageField("Profile Pic", blank=True, upload_to="profile/images/")

	def __unicode__(self):
		return self.user.username


class Skills(models.Model):

	student = models.ForeignKey(Student)
	skills = models.CharField(max_length=50, blank=True)

	def __unicode__(self):
		return self.skills

class Experience(models.Model):

	student = models.ForeignKey(Student)

	company = models.CharField(max_length=25, blank=True)
	profile = models.CharField(max_length=25, blank=True)
	location = models.CharField(max_length=20, blank=True)
	duration = models.CharField(max_length=20, blank=True)

	def __unicode__(self):
		return self.company
