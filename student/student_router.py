

class StudentRouter(object):
    """
    A router to control all database operations on models in the
    student application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read student models go to student_db.
        """
        if model._meta.app_label == 'student':
            return 'student_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write student models go to student_db.
        """
        if model._meta.app_label == 'student':
            return 'student_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the student app is involved.
        """
        if obj1._meta.app_label == 'student' or \
           obj2._meta.app_label == 'student':
           return True
        return None

    def allow_syncdb(self, db, model):
        """
        Make sure the student app only appears in the 'student_db'
        database.
        """
        if db == 'student_db':
            return model._meta.app_label == 'student'
        elif model._meta.app_label == 'student':
            return False
        return None