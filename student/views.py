from django.shortcuts import render
import urllib
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import UserForm, StudentForm, SkillsForm, ExperienceForm        #explicit relative import, for more info read "2 scoops of django".
from .models import Student, Skills, Experience
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import auth
import json
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers


def home(request):
    if request.user.is_authenticated():
        username = request.user.username
        
        return HttpResponseRedirect("/student/%s/profile"  % urllib.quote(username) )

    
    return render(request, 'home.html')



def register(request):

    context = RequestContext(request)

    if request.method =='POST':

        form = UserForm(request.POST)
        if form.is_valid():

            username=form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = User.objects.create_user(username=username, password=password )
            user.save()

            #authenicate user after registeration
            current_user = authenticate(username=username, password=password)

            if current_user is not None:

                auth.login(request, current_user)
            
                return HttpResponseRedirect("/student/%s/profile" % urllib.quote(username) )
        else:
            print form.errors

            return render(request, 'home.html', context_instance=context)


def login(request):

    context = RequestContext(request)

    if request.method == 'POST':
        
        username = request.POST['username']
        password = request.POST['password']

        student = authenticate(username=username, password=password)
        if student is not None:
            auth.login(request, student)
            
            return HttpResponseRedirect("/student/%s/profile" % urllib.quote(username) )

        else:

            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

            #return HttpResponseRedirect(reverse(invalid_login))
    else:
        
        return render_to_response('home.html', context)



@login_required
def profile(request, username):

    current_students = all_users()

    user = get_object_or_404(User, username=username) #Calls get() on a given model manager

    people, new_entry = Student.objects.get_or_create(user=user)

    data = {}
    data['edit'] = 'True'
    data['students'] = current_students

    data['username'] = request.user

    profile =  Student.objects.get(user = request.user)

    data['name'] = profile.name
    data['homepage_url'] = profile.homepage_url

    try:
        Skills.objects.filter(student=profile)

        skill = Skills.objects.filter(student= profile)
        
        #data['sid'] = i.sid
        data['skill'] = skill

    except (Skills.DoesNotExist):
        data['title'] = ''
    try:
        Experience.objects.filter(student= profile)
        exp = Experience.objects.filter(student= profile)

        data['exp'] = exp
       
    except (Experience.DoesNotExist):
        data['company'] = ''
        data['profile'] = ''
        data['location'] = ''
        data['duration'] = ''

    return render_to_response( 'dashboard.html', {'info': data } )


def all_users():

    all_users = User.objects.order_by('student')

    return all_users

def edit(request, username):
    context = RequestContext(request)

    return render_to_response('edit.html', context)

@csrf_exempt
@login_required
def edits(request):

    context = RequestContext(request)

    data = {}

    #data['username'] = request.user

    profile =  Student.objects.get(user = request.user)

    data['name'] = profile.name
    data['homepage_url'] = profile.homepage_url

    try:
        Skills.objects.filter(student=profile)

        skill = Skills.objects.filter(student= profile)
        
        skill_list = []

        for i in skill:

            all_skills ={}
            all_skills['id'] = i.pk
            all_skills['skills'] = i.skills
            #all_skills['skills'] = i.skill

            skill_list.append(all_skills)

        data['skill'] = skill_list


    except (Skills.DoesNotExist):
        data['skill'] = ''


    try:
        Experience.objects.filter(student= profile)
        exp = Experience.objects.filter(student= profile)

        exp_list=[]
        for j in exp:

            all_exp ={}
            all_exp['company'] = j.company
            all_exp['profile'] = j.profile
            all_exp['location']= j.location
            all_exp['duration']= j.duration
            #all_skills['skills'] = i.skill

            exp_list.append(all_exp)

        data['exp'] = exp_list
       

    except (Experience.DoesNotExist):
        data['exp'] = ''

    #skills = serializers.serialize('json', skill)
    #expr = serializers.serialize('json', exp)

    #info = json.dumps(list(data), cls=DjangoJSONEncoder)

    #print data

    return HttpResponse(json.dumps(data),content_type='application/json; charset=utf8')


"""return HttpResponse( json.dumps({'nombre': inmueble.nombre,
        'descripcion': inmueble.descripcion, 'foto' : inmueble.foto }),
        content_type='application/json; charset=utf8') """





def logout(request):
    auth.logout(request)
    return render_to_response('home.html')

@login_required
def invalid_login(request):

   return render_to_response('invalid_login.html')


@login_required
def hackathons(request):

    return render_to_response('hackathons.html')


@login_required
def editbio(request):

    user = User.objects.get(username=request.user)
    student = Student.objects.get(user = user)

    pid = student.id

    if pid:

        instance_id = get_object_or_404(Student, pk= pid)

    else:
        instance_id = Student.objects.get(user= user)


    if request.method == 'POST':

        basic_form = StudentForm(request.POST, instance = instance_id)

        if basic_form.is_valid():
            basic_form.save()
            
            response_dict = {} 
            y = "success"

            response_dict.update({'server_response': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print basic_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))

@login_required
def addskills(request):
    user = request.user
    student = Student.objects.get(user=request.user)
    pid = 1

    if pid:
        instance = get_object_or_404(Skills, pk=pid)
        print 1
    else:
        instance = student
        print 2
    
    if request.method == 'POST':

        skill_form = SkillsForm(request.POST, instance = instance)
        
        if skill_form.is_valid():

            skill_form = skill_form.save()
            
            response_dict = {} 
            y = "success"

            response_dict.update({'result': y })  

        else:
            
            print skill_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' }) 


        return HttpResponse(json.dumps(response_dict))


@login_required
def addexp(request):

    student = Student.objects.get(user=request.user)

    if request.method == 'POST':
        
        exp_form = ExperienceForm(request.POST)
        if exp_form.is_valid():
            
            exp = exp_form.save(commit=False)
            exp.student = student
            exp.save()

            response_dict = {} 
            y = "success"

            response_dict.update({'result': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print exp_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))

def test(request):
        return render_to_response('test.html', {'form':UserForm()})


@login_required
def editavatar(request):
    user = User.objects.get(username=request.user)    
    student = Student.objects.get(user= user)

    if request.method == 'POST':

        print request.POST
        print request.FILES

        avatar_form = StudentForm(request.POST, request.FILES, instance = student)

        if avatar_form.is_valid():
            avatar_form.save()
            
            response_dict = {} 
            y = "success"

            response_dict.update({'server_response': y })  

            return HttpResponse(json.dumps(response_dict))
        else:
            
            print basic_form.errors

            response_dict = {} 

            response_dict.update({'found an error': 'found an error' })  

            return HttpResponse(json.dumps(response_dict))



"""
@csrf_exempt
@login_required
def editskill(request):

    s_id = request.POST['id'] 

    resp = {}
    skills = Skills.objects.get(pk= s_id)
    resp['title'] = skills.title

    return HttpResponse(json.dumps(resp))

"""