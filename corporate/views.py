from django.shortcuts import render
import urllib
from django.shortcuts import render_to_response, redirect, render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context import RequestContext
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

#from .forms import         #explicit relative import, for more info read "2 scoops of django".
#from .models import Student, Skills, Experience
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import auth
import json


def home(request):

	print 'home'

	if request.user.is_authenticated():
		username = request.user.username

		print username

		return HttpResponseRedirect("/corporate/%s/profile"  % urllib.quote(username) )

	else:

		return render(request, 'c_login.html')


def login(request):
	
	context = RequestContext(request)

	if request.method == 'POST':

		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)

		if user is not None:
	      
			auth.login(request, user)
			
			return HttpResponseRedirect("/corporate/%s/profile" % urllib.quote(username) )

		else:

			print "Invalid login details: {0}, {1}".format(username, password)

			return HttpResponse("Invalid login details supplied.")

	else:

		return render_to_response('c_login.html', context)


@login_required
def dashboard(request, username):


	return render_to_response('c_dashboard.html')

