from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),

    """
    url(r'^login', views.login, name='login'),
    url(r'^logout', 'django.contrib.auth.views.logout',
                        {
                            'next_page': '/'}),

    url(r'^student/(?P<username>\w+)/profile', views.profile),
    url(r'^register', views.register, name='register'),
    #url(r'^profile/', views.profile, name='Profile'),
    url(r'^invalid_login/', views.invalid_login, name='invalid_login'),
    url(r'^hackathons', views.hackathons, name='hackathons'),
    url(r'^student/(?P<username>\w+)/edit/$', views.edit, name='edit'),
    url(r'^editbio', views.editbio),
    url(r'^editavatar', views.editavatar),
    url(r'^addskill', views.addskills),
    url(r'^addexp', views.addexp),

    url(r'^test', views.test),
    url(r'^verify_username', views.verify_username)



"""
)
