
GeekLab
--------

the geeklab platform.


Message for contributors
-------------------------

* create a separate branch (preferably your name) for making changes. 

* asssign a recognizable commit message before pushing your code. In case of conflicts these commit messages will be our saviour. :)

* Fully test the functionalities locally and do 'git pull' before pushing your new changes.

* add relevant comment(s) in the code for others to understand. Point out if specific line(s) of code is confusing for you.

* create issues if there is a relevant bug in the project.



Things to work on
-------------------

* Frontend validation and footer issue
* geek connect (the atom unit)
		# corporate page
		#student interaction
		
* make frontend more decent, add colour to section and bottom(s)
* create code for 'similar users' section
* github integration (sync API)
* Forum
* Chat app
* notifications
* follow feature with users


Installation Instructions :
--------------------------
Install all the dependencies from requirements.txt as "pip install -r requirements.txt"

* an overview of the app can be seen at http://geeklab.herokuapp.com




