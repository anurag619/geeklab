from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()
import geeklab.mysite.views
import student.views
import corporate.views

urlpatterns = patterns('',

    url(r'^$', 'geeklab.mysite.views.home'),
    url(r'^student/$', 'student.views.home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^corporate/$', 'corporate.views.home'),
    #url(r'^academic/', include('acamedic.urls')),

    url(r'^student/login$', 'student.views.login'),
    url(r'^logout/', 'django.contrib.auth.views.logout',
                        {'next_page': '/student'}),
    url(r'^student/(?P<username>\w+)/profile$', 'student.views.profile'),
    url(r'^student/register$', 'student.views.register'),
    #url(r'^profile/', views.profile, name='Profile'),
    url(r'^student/invalid_login$', 'student.views.invalid_login'),
    url(r'^student/hackathons$', 'student.views.hackathons'),
    url(r'^student/edit/$', 'student.views.edits'),
    url(r'^student/(?P<username>\w+)/edit/$', 'student.views.edit'),
    url(r'^student/editbio', 'student.views.editbio'),
    url(r'^student/editavatar$', 'student.views.editavatar'),
    url(r'^student/addskill', 'student.views.addskills'),
    #url(r'^student/editskill', 'student.views.editskill'),
    url(r'^student/addexp$', 'student.views.addexp'),
	url(r'^student/test$', 'student.views.test'),

	url(r'^corporate/login$', 'corporate.views.login'),
	url(r'^corporate/(?P<username>\w+)/profile/$', 'corporate.views.dashboard'),
    




)


