

$(function editskill(){

	$(".edit-skill-form").submit(function() {
		
		$.ajax({
		url : "/addskill",
		type : "POST",
		dataType: "json",

		data : $(this).serialize(),
						
		success : function(json) {
				$('#skill-success').html( 'Server Response: ' + json.result);
			},

		error: function(data) {
                $("#skill-success").html("Something went wrong!");
            }

		});

		return false;

	});

});


$(function tab() {

    $('#myTab a:first').tab('show')
  });


/* add skills */

$(function addmoreskill(){

	$('.addmore-skill').on('click', function(e){
						e.preventDefault();
		    			
		    			$('.skill-form').show();

		    		
							});


	$( ".cancel-skillform" ).on('click', function(e) {
						e.preventDefault();
						$( ".skill-form" ).hide();
					});

	});

$(function experience(){
	
			$('.addmore-exp').on('click', function(e){
    			e.preventDefault();
    			$('.exp-form').show();
					});
			

			$( ".cancel-expform" ).click(function(e) {
				e.preventDefault();
				$( ".exp-form" ).hide();
			});

	});


/* ------name and website form data ----  */

$(function editinfo(){

	$(".inline-form").submit(function() {
		
		$.ajax({
		url : "/editbio",
		type : "POST",
		dataType: "json",

		data : $(this).serialize(),			
			
		success : function(json) {
				$('.result').html( 'Server Response: ' + json.server_response);
			},

		error: function(data) {
                $(".result").html("Something went wrong!");
            }

		});

		return false;

	});

});


/* editing the avatar */

$('#add-picture-form').submit(function() { 
    //var filename = $("#upload-photo").val();
    var photo = $("editavatar").val(); 
    var file  = photo.files[0];

	$.ajax({ 
	    type: "POST",
	    url: "/editavatar",
	    enctype: 'multipart/form-data',
	    data: {'file': file},

	    success: function(){
	       alert( "Data Uploaded: ");
	    }
});

    return false;   
}); 


/* add experience form  */

$(function editexp(){

	$(".edit-exp-form").submit(function() {
		
		$.ajax({
		url : "/addexp",
		type : "POST",
		dataType: "json",

		data : $(this).serialize(),
			
		success : function(json) {
				$('#exp-success').html( 'Server Response: ' + json.result);
			},

		error: function(data) {
                $("#exp-success").html("Something went wrong!");
            }

		});

		return false;

	});

});