
$(document).ready(function(){$edit()})

$edit = function (){


	$.ajax({
	url : "/student/edit",
	type : "GET",
					
	success : function(info) {

		var a = $(".username");
		var b = $('.homepage_url');
		a.text(info.name),
		a.val(info.name),
		b.attr('href',  info.homepage_url),
		b.text(info.homepage_url),
		b.val( info.homepage_url);

		var c = (info.skill).length;

		//alert(info.skill[0]);
		var elem = '';
		for (i=0; i<c ;i++){
			
			elem += ( "<span>"+info.skill[i].skills+"</span>");

		}
		$('.skills').html(elem);
		
		},

/* 
{
'homepage_url': u'http://anuragkr.in/', 
'skill': [{'skills': u'', 'id': 1},
'name': u'anurag kumar', 
'exp': [
	{'profile': u'intern', 
	'duration': u'1 yr', 'company': u'YS', 'location': u'kol'}]
	}

*/

	error: function(data) {
            $("#skill-success").html("Something went wrong!");
        }

	});

	return false;

};

$(function tab() {

    $('#myTab a:first').tab('show')
  });

/* add skills */

$(function addmoreskill(){

	$('.addmore-skill').on('click', function(e){
						e.preventDefault();
		    			
		    			$('.skill-form').show();

		    		
							});

	$( ".cancel-skillform" ).on('click', function(e) {
						e.preventDefault();
						$( ".skill-form" ).hide();
						$( ".skills-form" ).hide();
					});

	});

$(function experience(){
	
			$('.addmore-exp').on('click', function(e){
    			e.preventDefault();
    			$('.exp-form').show();
					});
			$( ".cancel-expform" ).click(function() {
				
				$( ".exp-form" ).hide();
				
			});
	});

/*----- edit a perticular skill ----- */

$(function editoneskill(){


	$.ajax({
	
		url : "/student/edit",
		type : "GET",
		
		success : function(json) {

			var a = $('.skills-form');
			
			//$('.skill-form').show();
			
			//$('.skill-form').field('skills', json.);
			//$('.skill-form').field('skill-id', id) 

			},

		error: function(data) {
	            alert('fail')
	        }

		});
				    		
});	

/* ------name and website form data ----  */

$(function editinfo(){

	$(".inline-form").submit(function(e) {
		e.preventDefault();

		var u = '/student/editbio'
		$.ajax({
		url : u,
		type : "POST",
		data : $(this).serialize(),	
		dataType: "json",		
			
		success : function(json) {
				$('.result').html( 'Server Response: ' + json.server_response);
				$edit();
				
			},

		error: function(data) {
                $(".result").html("Something went wrong!");
            },
		});
		return false;
	});

});

$(function editskill(){

	$(".edit-skill-form").submit(function() {

		$.ajax({
		url : "/student/addskill",
		type : "POST",
		dataType: "json",
		data : $(this).serialize(),
						
		success : function(json) {
				$(".edit-skill-form").fadeOut( "slow" );
				//location.reload()
				
				$('#skill-success').html('Server Response: ' + json.result);
				$edit();
			},

		error: function(data) {
                $("#skill-success").html("Something went wrong!");
            }

		});

		return false;
	});
}); 


/* editing the avatar */

$('#add-picture-form').submit(function() { 
    //var filename = $("#upload-photo").val();
    var photo = $("editavatar").val(); 
    var file  = photo.files[0];

	$.ajax({ 
	    type: "POST",
	    url: "/student/editavatar",
	    enctype: 'multipart/form-data',
	    data: {'file': file},

	    success: function(){
	       alert( "Data Uploaded: ");
	    }
});

    return false;   
}); 


/* add experience form  */

$(function editexp(){

	$(".edit-exp-form").submit(function() {
		
		$.ajax({
		url : "/student/addexp",
		type : "POST",
		dataType: "json",

		data : $(this).serialize(),
			
		success : function(json) {
				$('#exp-success').html( 'Server Response: ' + json.result);
			},

		error: function(data) {
                $("#exp-success").html("Something went wrong!");
            }

		});

		return false;

	});

});

$(function() {

		 var availableTags = [
			"CSS",
			"C++",
			"Jquery",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
			];

	 	function split( val ) {
			return val.split( /,\s*/ );
		}

		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#skills" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
			if ( event.keyCode === $.ui.keyCode.TAB &&
			$( this ).data( "ui-autocomplete" ).menu.active ) {
			event.preventDefault();
			}
			})

		.autocomplete({
			minLength: 0,
			source: function( request, response ) {
			// delegate back to autocomplete, but extract the last term
			response( $.ui.autocomplete.filter(
			availableTags, extractLast( request.term ) ) );
			},

		focus: function() {
			// prevent value inserted on focus
			return false;
		},

		select: function( event, ui ) {
			var terms = split( this.value );
			// remove the current input
			terms.pop();
			// add the selected item
			terms.push( ui.item.value );
			// add placeholder to get the comma-and-space at the end
			terms.push( "" );
			this.value = terms.join( ", " );
			return false;
			}
		});
});


//field function to get/set input values of any type of input
(function () {
    $.fn.field = function (inputName, value)
    {
        console.log('field called...');
        console.log($(this));

        console.log(typeof inputName);

        if (typeof inputName !== "string") return false;
        var $inputElement = $(this).find("[name=" + inputName + "]");
        // var $inputElement = $(this); //direct mapping with no form context

        console.log($inputElement);

        if (typeof value === "undefined" && ($inputElement.length >= 1) )
        {
            switch ($inputElement.attr("type"))
            {
                case "checkbox":
                    return $inputElement.is(":checked");
                    break;
                case "radio":
                    var result;
                    $inputElement.each(function (i, val) {
                        if ($(this).is(":checked")) result = $(this).val()
                    });
                    return result;
                    break;
                default:
                    return $inputElement.val();
                    break;
            }
        }
        else
        {
            switch ($inputElement.attr("type"))
            {
                case "checkbox":
                    $inputElement.attr({
                        checked: value
                    });
                    break;
                case "radio":
                    $inputElement.each(function (i) {
                        if ($(this).val() == value) $(this).attr({
                            checked: true
                        })
                    });
                    break;
                case undefined:
                    $(this).append('<input type="hidden" name="' + inputName + '" value="' + value + '" />');
                    break;
                default:
                    $inputElement.val(value);
                    break;
            }
            return $inputElement;
        }
    }
})();